// swift-tools-version:4.1
import PackageDescription

let package = Package(
    name: "kdeconnect share",
    dependencies: [
        .package(
            url: "https://github.com/Inokinoki/DBus.git",
            .branch("master")
        )
    ],
    swiftLanguageVersions: [4]
)
