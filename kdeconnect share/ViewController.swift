//
//  ViewController.swift
//  kdeconnect share
//
//  Created by cici on 2019/7/11.
//  Copyright © 2019 Inoki. All rights reserved.
//

import Cocoa
import DBus
import CDBus

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        do {
            let conn = try DBusConnection.init(busType: .session);
            if conn.connected {
                NSLog("Connected");
                
                NSLog("Test execution");
                let msg = try DBusMessage.init(type: .methodCall);
                try msg.setDestination(DBusBusName.init(rawValue: "org.kde.kdeconnect"))
                try msg.setPath(DBusObjectPath.init(rawValue: "/modules/kdeconnect"))
                try msg.setInterface(DBusInterface.init(rawValue: "org.kde.kdeconnect.daemon"))
                try msg.setMember(DBusMember.init(rawValue: "devices"))
                try msg.setSender(DBusBusName.init(rawValue: "KDE Connect Share"))
                
                try msg.append(DBusMessageArgument.boolean(false))
                try msg.append(DBusMessageArgument.boolean(false))
                
                let reply = try conn.sendWithReply(message: msg)
                
                reply?.block()
                print(reply?.replyMessage?.type)
                if reply?.replyMessage?.type == .error {
                    print(reply?.replyMessage?.errorName?.rawValue)
                } else {
                    reply?.replyMessage?.forEach { print($0) }
                }
            } else {
                NSLog("Not Connected");
            }
        } catch {
            NSLog("error");
        }
        
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

